module.exports = function(grunt, data) {
  return {
    scss: {
      files: 'public/scss/**/*.scss',
      tasks: ['sass:dev', 'postcss']
    }
  };
}

// Playground for me to mess around with the game code
// Later this should be easily implementable by doing that <script src="public/js/solitaire.js"></script> thingy

// Cards are intentified by numbers: 0, 13, 26 and 39 are aces and 1, 14, 27 and 40 are 2s etc.
// Types: 0-12 are hearts, 13-25 are diamonds, 26-38 are clubs, and 39-51 are spades
// Advantage of the system: card % 13 = typeless version of iteself, floor(card / 13) gives you the type of the card,
// card >= 26 is true if the card is black, false if it's red

// If I want to check if a card (x) can be dragged onto another card (y) in the tableu,
// I can simply check if (x+1)%13 == y%13 && (x>=26)!=(y>=26)

// 13 arrays for all the cards to go in.
// At game start the cards will be shuffled into the deck (stack[0]) and then distributed according to game rules
var stacks = [[],[],[],[],[],[],[],[],[],[],[],[],[]];
var cardAmount = 52;

// Set up a fresh new board
function resetBoard(){

  // Reset stacks
  for (var i = stacks.length - 1; i >= 0; i--) {
    stacks[i] = [];
  }

  // Push 52 new cards to the deck
  while (stacks[0].length < cardAmount)
    stacks[0].push(stacks[0].length+1);

  // Shuffle the deck (works similar to insertion sort)
  var j, x;
    for (var i = cardAmount; i; i--) {
        j = Math.floor(Math.random() * i);
        x = stacks[0][i - 1];
        stacks[0][i - 1] = stacks[0][j];
        stacks[0][j] = x;
    }

    // Redistribute deck cards across the tableu
    for (var i = 6; i < 13; i++){
      for (var j = 0; j <= i-6; j++){
        stacks[i].push(stacks[0].pop());
      }
    }
}

resetBoard();


// Create a string representation of the board (for debugging)
var boardstring = "";
var denoms = ["Deck:    ", "Pile:    ", "Top1:    ", "Top2:    ", "Top3:    ", "Top4:    ", "Column1:    ",
"Column2:    ", "Column3:    ", "Column4:    ", "Column5:    ", "Column6:    ", "Column7:    "]
for (var i = 0; i < stacks.length; i++) {
  boardstring += denoms[i] + stacks[i] + "\n";
}

// Print the full board as an alert
alert(boardstring);